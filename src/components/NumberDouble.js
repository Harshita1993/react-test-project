import React, { Fragment, useEffect, useState } from "react";

import classes from "./NumberDouble.module.css";

const NumberDouble = (props) => {
  const [counter, setCounter] = useState(2);
  const [numbersList, setNumbersList] = useState([counter]);
  const [content, setContent] = useState(null);
  const [message, setMessage] = useState("");

  const numberDoubleHandler = () => {
    setCounter(counter * 2);
    if (counter <= 2000 && counter > 0) {
      setMessage(<p className={classes.success}>You did it well</p>);
    } else {
      setMessage(<p className={classes.fail}>Limit Reached !</p>);
    }
  };

  useEffect(() => {
    const numbersListArray = [];
    if (counter !== 2 && counter <= 2000) {
      numbersListArray.push(counter);
    }
    setNumbersList([...numbersList, numbersListArray]);
  }, [counter]);

  const showNumbersList = () => {
    let contentt = numbersList.map((item) => <li key={item}>{item}</li>);
    setContent(contentt);
  };

  return (
    <Fragment>
      <div>
        {counter < 2000 ? (
          <button onClick={numberDoubleHandler}>Double Me</button>
        ) : (
          <button disabled>Double Me</button>
        )}
        <p>The current number is: {counter}</p>
        {message}
      </div>
      <div>
        <button onClick={showNumbersList}>Show List</button>
        <ul className={classes["list-style"]}>{content}</ul>
      </div>
    </Fragment>
  );
};

export default NumberDouble;
