import './App.css';
import NumberDouble from './components/NumberDouble';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <NumberDouble />
      </header>
    </div>
  );
}

export default App;
